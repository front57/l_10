const btns = document.querySelectorAll('.steps__nav-btn');
const cards = document.querySelectorAll('.steps__card');



btns.forEach((e,idx) => e.addEventListener('click', function(){
    dropActiveCls();
    verticalLineHeight();
    this.classList.add('active');
    cards[idx].scrollIntoView({behavior: "smooth"});

}));

function dropActiveCls(){
    btns.forEach(e => e.classList.remove('active'));
}

window.addEventListener('load', verticalLineHeight);
window.addEventListener('resize', verticalLineHeight);

function verticalLineHeight(){
    const lastBullet = document.querySelector('.steps__btn-bullet--last');
    const firstBullet = document.querySelector('.steps__btn-bullet--first');
    const line = document.querySelector('.steps__v-line');
    const lastBulletRect = lastBullet.getBoundingClientRect();
    const firstBulletRect = firstBullet.getBoundingClientRect();
    const lineHeight = lastBulletRect.top - firstBulletRect.bottom ;
    line.style.height = lineHeight + 23 +  'px';
}

