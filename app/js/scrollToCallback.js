const arrowBtn = document.querySelector('.hero__arrow-bottom');
const needleForm = document.querySelector('.callback__form');

arrowBtn.addEventListener('click', function () {
    needleForm.scrollIntoView({behavior: "smooth", block: "center"});
})