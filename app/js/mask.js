const modalPhoneInput = document.querySelector('.modal__input--phone');
const callbackPhoneInput = document.querySelector('.callback__f-phone-number');
const maskOptions = {
    mask: '+ {7} (000) 000 00 00'
};
const mask1 = IMask(modalPhoneInput, maskOptions);
const mask2 = IMask(callbackPhoneInput, maskOptions);