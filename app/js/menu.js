window.onload = () => {
    const burger = document.querySelector('.hamburger');
    const header_menu = document.querySelector('.nav__menu');
    const body = document.querySelector('body');
    const clsBtn = document.querySelector('.nav__close');
    burger.addEventListener('click',  () => {
        burger.classList.toggle('active');
        header_menu.classList.toggle('active');
        body.classList.toggle('lock');
    });
    clsBtn.addEventListener('click', function(){
        burger.classList.remove('active');
        header_menu.classList.remove('active');
        body.classList.remove('lock');
    })
};