const swiper = new Swiper(".swiper", {

    breakpoints: {
        320: {
            slidesPerView: 1,
            spaceBetween: 20,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
        },
        992: {
            slidesPerView: 2,
            spaceBetween: 0,
        },
    }
});

window.addEventListener('load', function(){
   if(window.innerWidth > 992){
       document.querySelector('.swiper-wrapper').classList.add('disabled');
   }
});

window.addEventListener('resize', function(){
    const slider = document.querySelector('.swiper-wrapper');
    if ( window.innerWidth > 992){
        slider.classList.add('disabled');
    }else{
        slider.classList.remove('disabled');
    }
})
