const callbackBtn = document.querySelector('.callback__btn');
const chxBox = document.querySelector('.callback__checkbox');
const label = document.querySelector('.callback__checkbox-label');

label.addEventListener('click', function(e){
    if(chxBox.checked ){
        callbackBtn.disabled = !callbackBtn.disabled;
    }
    if(e.target.classList.contains('callback__private-policy')){
        e.preventDefault();
    }
})
