const displayImg = (entries, observer) => {
    entries.forEach( entry => {
        if(entry.isIntersecting){
            entry.target.classList.add('normalize-h-element');
        }
    });
};

const displayTitle = (entries, observer) => {
    let sleep = 1000;
    entries.forEach( entry => {
        if(entry.isIntersecting){
            setTimeout(function(){
                entry.target.classList.add('normalize-v-element');
            }, sleep);
        }
        sleep += 450;
    });
};

const moveArrow = (entries, observer) => {
    entries.forEach( entry => {
        if(entry.isIntersecting){
            setTimeout(function(){
                entry.target.style.animation = entry.target.dataset.animate;
                entry.target.style.opacity = '1';
            }, 500);
        }
    });
};

const displayCommonTitle = (entries, observer) => {
    entries.forEach( entry => {
        if(entry.isIntersecting){
            setTimeout(function(){
                entry.target.classList.add('normalize-h-element');
            }, 200);
        }
    });
};

const displayAdvCards = (entries, observer) => {
    let sleep = 300;
    const currentWidth = window.innerWidth;
    entries.forEach(entry => {
        if(entry.isIntersecting){
            setTimeout(function(){
                if(currentWidth > 768){
                    entry.target.classList.add('normalize-v-element');
                }else{
                    entry.target.classList.add('normalize-element');
                }
            }, sleep);
        }
        sleep += 400;
    });
};

const displayBtn = function (entries, observer) {
    entries.forEach(entry => {
        if(entry.isIntersecting){
            setTimeout(function () {
                entry.target.classList.add('normalize-opacity');
            }, 300);
        }
    });
};

const displayBenCards = function (entries, observer) {
    let sleep = 200;
    entries.forEach(entry => {
        if(entry.isIntersecting){
            setTimeout(function () {
                entry.target.querySelector('.benefits__img-wrapper').classList.add('normalize-h-element');
                entry.target.querySelector('.benefits__card-desc').classList.add('normalize-h-element');
            }, sleep);
        }
        sleep += 300;
    });
}

const setCurrentStep = function (entries, observer) {
    const navElems = document.querySelectorAll('.steps__nav-btn');
    entries.forEach((entry) => {
        if(entry.isIntersecting){
            dropActiveCls();
            navElems[+entry.target.dataset.id].classList.add('active');
        }
    });
};

const displayClbImages = function (entries, observer) {
    let sleep = 250;
    entries.forEach(entry => {
        if(entry.isIntersecting){
            setTimeout(function () {
                entry.target.classList.add('normalize-element');
            }, sleep);
        }
        sleep += 350;
    })
}

const opts = {};

const observer = new IntersectionObserver(displayImg, opts);
const elements = document.querySelectorAll('.hero__img-bg');

elements.forEach( e => {
    observer.observe(e);
});

const titleObserver = new IntersectionObserver(displayTitle, opts);
const elementsVertical = document.querySelectorAll('.hero__title, .hero__info, .hero__btn, .hero__desc');

elementsVertical.forEach( e => {
    titleObserver.observe(e);
});

const arrowObserver = new IntersectionObserver(moveArrow, opts);
const arrowElement = document.querySelector('.hero__arrow-bottom');

arrowObserver.observe(arrowElement);

const allTitleObserver = new IntersectionObserver(displayCommonTitle, opts);
const titles = document.querySelectorAll('.title-animation');

titles.forEach( e => {
    allTitleObserver.observe(e);
});

const advCardsObserver = new IntersectionObserver(displayAdvCards, opts)
const advantagesCards = document.querySelectorAll('.advantages__card');

advantagesCards.forEach(e => {
   advCardsObserver.observe(e);
});

const btnsObserver = new IntersectionObserver(displayBtn, opts);
const hiddenBtns = document.querySelectorAll('.benefits__btn, .advantages__btn');

hiddenBtns.forEach(e => {
    btnsObserver.observe(e);
})

const benefitsObserver = new IntersectionObserver(displayBenCards,  opts);
const benefitsItems = document.querySelectorAll('.benefits__card');

benefitsItems.forEach(e => {
    benefitsObserver.observe(e);
});

const stepsObserver = new IntersectionObserver(setCurrentStep, {threshold: .5});
const stepsCards = document.querySelectorAll('.steps__card');

stepsCards.forEach(e => {
    stepsObserver.observe(e);
});


const clbImagesObserver = new IntersectionObserver(displayClbImages, {threshold: .1});
const clbImages = document.querySelectorAll('.callback__img-wrapper');


clbImages.forEach(e => {
    clbImagesObserver.observe(e);
})